$(document).ready(function() {
    Galleria.loadTheme(Drupal.settings.mct_gallery.theme);
    $("#galleria").galleria({
	width: 680,
	height: Math.min($(window).height() - 80, 540)
    });
    var gal = Galleria.get("#galleria")[0];
    var resetGal = function() {
	gal.splice(0,1);
	for (i=0; i<Drupal.settings.mct_gallery.photos.length; i++) {
	    gal.push(Drupal.settings.mct_gallery.photos[i]);
	}
    }
    if (typeof(gal.getIndex()) == "number") {
	resetGal();
    }
    else {
	Galleria.utils.wait({
	    until: function() {
		return typeof(gal.getIndex()) == "number";
	    },
	    success: function() {
		resetGal();
	    },
	    error: function() {
		console.log('error');
	    },
	    timeout: 4000
	});
    }
});

