<?php

/**
 * Implementation of hook_user_default_roles().
 */
function mct_user_default_roles() {
  $roles = array();

  // Exported role: Forum Moderator
  $roles['Forum Moderator'] = array(
    'name' => 'Forum Moderator',
  );

  // Exported role: admin
  $roles['admin'] = array(
    'name' => 'admin',
  );

  // Exported role: alumni
  $roles['alumni'] = array(
    'name' => 'alumni',
  );

  // Exported role: anonymous user
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
  );

  // Exported role: authenticated user
  $roles['authenticated user'] = array(
    'name' => 'authenticated user',
  );

  // Exported role: exec
  $roles['exec'] = array(
    'name' => 'exec',
  );

  // Exported role: team
  $roles['team'] = array(
    'name' => 'team',
  );

  return $roles;
}
