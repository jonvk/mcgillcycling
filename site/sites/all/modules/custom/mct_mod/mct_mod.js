Drupal.behaviors.mct_mod = function () {
  $('body.front-page div#content-area div.content').hide();
  $('body.front-page ul.tabs.nav-primary a.active span').html('About Us');
  $('body.front-page ul.tabs.nav-primary a.active').click(function() {
    $('body.front-page ul.tabs.nav-primary a.active span').toggleClass('photos');
    if ($('body.front-page ul.tabs.nav-primary a.active span').hasClass('photos')) {
      $('body.front-page ul.tabs.nav-primary a.active span').html('Photos');
    }
    else {
      $('body.front-page ul.tabs.nav-primary a.active span').html('About Us');
    }
    $('body.front-page div#content-area div.content').slideToggle();
    $('body.front-page div.region-content-bottom #block-views-front_images-block_1').slideToggle();
    return false;
  });
}
