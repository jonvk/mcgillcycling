Drupal.behaviors.mct_user = function (context) {
  $('a#top-login-link:not(.mct-user-processed)', context).addClass('mct-user-processed').each(function () {
    $(this).click(function() {
      $('div#top-login-form').toggle(320);
      $('div#top-login-form input#edit-name-1').focus();
      return false;
    });
  });
};
