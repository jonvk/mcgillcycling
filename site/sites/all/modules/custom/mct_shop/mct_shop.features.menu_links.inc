<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function mct_shop_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:shop
  $menu_links['primary-links:shop'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'shop',
    'router_path' => 'shop',
    'link_title' => 'Shop',
    'options' => array(
      'attributes' => array(
        'title' => 'Shop',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Shop');


  return $menu_links;
}
