<div id="mct-order-<?php print $order['oid'] ?>" class="mct-order">
  <div class="mct-order-info">
    <h2>Order Information</h2>
    <div class="mct-order-id">
      <label>Order ID: </label>
      <?php print $order['oid']; ?>
    </div>
    <div class="mct-order-name">
      <label>Name: </label>
      <?php print $order['name']; ?>
    </div>
    <?php if ($order['phone']): ?>
    <div class="mct-order-phone">
      <label>Phone: </label>
      <?php print $order['phone']; ?>
    </div>
    <?php endif; ?>
    <?php if ($order['shipping']): ?>
    <div class="mct-order-shipping">
      <label>Shipping: </label>
      <?php print $order['Shipping']; ?>
    </div>
    <?php endif; ?>
    <?php if ($operations[0]['comment']): ?>
    <div class="mct-order-comment">
      <label>Note: </label>
      <?php print $operations[0]['comment']; ?>
    </div>
    <?php endif; ?>
  </div><!-- End order info -->
  <div class="mct-order-items">
    <h2>Items</h2>
    <div class="mct-order-item-header">
      <div class="mct-order-item-title">Item</div>
      <div class="mct-order-item-size">Size</div>
      <div class="mct-order-item-quantity">Quantity</div>
      <div class="mct-order-item-value">Unit Value</div>
      <div class="mct-order-item-total">Total Value</div>
    </div>
    <?php foreach ($items as $item): ?>
    <div class="mct-order-item">
      <?php print $item; ?>
    </div>
    <?php endforeach; ?>
    <div class="mct-order-item mct-order-total">
      <div class="mct-order-item-title">Total:</div>
      <div class="mct-order-item-size">&nbsp</div>
      <div class="mct-order-item-quantity">&nbsp</div>
      <div class="mct-order-item-value">&nbsp</div>
      <div class="mct-order-item-total"><?php print $total; ?></div>
    </div>
  </div>
  <div class="mct-order-operations">
    <h2>Order history</h2>
    <div class="mct-order-operations-header">
      <div class="mct-order-operation-status">Status</div>
      <div class="mct-order-operation-date">Date</div>
      <div class="mct-order-operation-comment">Note</div>
    </div>
    <?php foreach ($operations as $operation): ?>
    <div class="mct-order-operation">
      <div class="mct-order-operation-status">
	<?php print _mct_shop_status_list($operation['status']); ?>
      </div>
      <div class="mct-order-operation-date">
	<?php print strftime('%c', $operation['timestamp']); ?>
      </div>
      <div class="mct-order-operation-comment">
	<?php print $operation['comment']; ?>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
  <div>
    <h2>Pay Now</h2>
    <div class="mct-shop-buy-now">
      <div class="mct-shop-buy-now-message">
	<?php print $message; ?>
      </div>
      <div class="mct-shop-buy-now-total">
	Paypal charge: <?php print $amount; ?>$
      </div>
      <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="amount" value="<?php print $amount; ?>">
	<input type="hidden" name="currency_code" value="CAD">	
	<input type="hidden" name="business" value="mcgillcycling@gmail.com">	
	<input type="hidden" name="item_name" value="Order <?php print $order['oid']; ?>">
	<input type="hidden" name="item_number" value="<?php print $order['oid']; ?>">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
      </form>
    </div>
  </div>
</div>
