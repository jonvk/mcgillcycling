<?php

function theme_mct_shop_order_item($item) {
  $node = node_load($item['item']);
  $ret = '<div class="mct-order-item-title">';
  $ret .= l($node->title, 'node/' . $node->nid);
  $ret .= '</div>' . "\n";
  $ret .= '<div class="mct-order-item-size">';
  $ret .= ($item['size'] ? $item['size'] : '-');
  $ret .= '</div>' . "\n";
  $ret .= '<div class="mct-order-item-quantity">';
  $ret .= $item['quantity'];
  $ret .= '</div>' . "\n";
  $ret .= '<div class="mct-order-item-value">';
  $ret .= theme('mct_shop_value', $item['value']);
  $ret .= '</div>' . "\n";
  $ret .= '<div class="mct-order-item-total">';
  $ret .= theme('mct_shop_value', $item['quantity']*$item['value']);
  $ret .= '</div>' . "\n";
  return $ret;
}

function theme_mct_shop_value($value) {
  return sprintf('%.2f$', $value/100);
}

function theme_mct_shop_order_row($row) {
  $customer = user_load($row['uid']);
  $ret = '<td class="mct-order-oid">' . l($row['oid'], 'shop/order/view/' . $row['oid']) . '</td>';
  $ret .= '<td class="mct_order-status">' . _mct_shop_status_list($row['status']) . '</td>';
  $ret .= '<td class="mct-order-user">' . theme('username', $customer) . '</td>';
  $ret .= '<td class="mct-order-name">' . $row['name'] . '</td>';
  $ret .= '<td class="mct-order-quantity">' . $row['pieces'] . '</td>';
  $ret .= '<td class="mct-order-total">' . theme('mct_shop_value', $row['total']) . '</td>';
  $ret .= '<td class="mct-order-date">' . strftime('%c', $row['timestamp']) . '</td>';
  return $ret;
}
