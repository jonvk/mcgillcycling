<?php

/**
 * Implementation of hook_imagecache_default_presets().
 */
function mct_shop_imagecache_default_presets() {
  $items = array(
    'mct_shop_item' => array(
      'presetname' => 'mct_shop_item',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '105',
            'height' => '105',
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function mct_shop_node_info() {
  $items = array(
    'mct_shop_items' => array(
      'name' => t('Shop Items'),
      'module' => 'features',
      'description' => t('Sellable items'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
