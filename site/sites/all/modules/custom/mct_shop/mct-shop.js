Drupal.behaviors.mct_shop = function(context) {
	var item_val = function (element) {
		var val = $('.mct-shop-list-price', element).html();
		val = val.substr(0,val.length - 1);
		var quantities = $('.mct-shop-list-quantity input', element);
		var total = 0;
		for (var i=0; i<quantities.length; i++) {
			total += quantities[i].value*val;
		}
		return total;
	}

	var order_total = function() {
		var items = $('#mct-shop-order-form .mct-shop-item');
		var total = 0;
		for (var i=0; i<items.length; i++) {
			total += item_val(items[i]);
		}
		return total.toFixed(2) + "$";
	}

	var update_total = function() {
		$('#mct-shop-order-value').html(order_total());
	}

	$('#mct-shop-order-form .mct-shop-list-quantity input:not-processed').change( update_total);

	if (Drupal.settings.mct_shop == undefined) {
		Drupal.settings.mct_shop = {};
	}
	if (Drupal.settings.mct_shop.total == undefined) {
		Drupal.settings.mct_shop.total = true,
		update_total();
	}
}
