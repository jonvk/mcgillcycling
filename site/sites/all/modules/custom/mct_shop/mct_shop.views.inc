<?php

/**
* Implementation of hook_views_data().
*/
function mct_shop_views_data() {
  $data = array();
  // Orders TABLE
  $data['mct_shop_order']['table'] = array(
    'group' => t('MCT Shop'),
    'title' => t('MCT Shop Orders'),
    'help' => t('MCT shop orders table. It holds an index of orders.'),
  );
  $data['mct_shop_order']['table']['base'] = array(
    'field' => 'oid',
    'title' => t('MCT shop order'),
    'help' => t('MCT shop orders.'),
  );
  $data['mct_shop_order']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['mct_shop_order']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The ID of the user who made this order.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
  );
  $data['mct_shop_order']['name'] = array(
    'title' => t('Name'),
    'help' => t('Full Name of person who made the order.'),
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['mct_shop_order']['phone'] = array(
    'title' => t('Phone Number'),
    'help' => t('Phone number of person who made the order.'),
    'field' => array(
      'handler' => 'views_handler_field', 
    ), 
  );
  $data['mct_shop_order']['shipping'] = array(
    'title' => t('Shipping Address'),
    'help' => t('Shipping address of person who made the order.'),
    'field' => array(
      'handler' => 'views_handler_field', 
    ), 
  );
  $data['mct_shop_order']['oid'] = array(
    'title' => t('Order ID'), 
    'help' => t('Order ID'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Operations Table
  $data['mct_shop_operations']['table'] = array(
    'group' => t('MCT Shop'),
    'title' => t('MCT shop operations'),
    'help' => t('MCT shop orders updates.'),
  );
  $data['mct_shop_operations']['table']['join'] = array(
    'mct_shop_order' => array(
      'left_field' => 'oid',
      'field' => 'oid',
    ),
  );
  $data['mct_shop_operations']['comment'] = array(
    'title' => t('Comment'),
    'help' => t('Comment'),
    'field' => array(
      'handler' => 'views_handler_field', 
    ), 
  );
  $data['mct_shop_operations']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The ID of the user who updated this order.'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
  );
  $data['mct_shop_operations']['status'] = array(
    'title' => t('Status'), 
    'help' => t('Order Status'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['mct_shop_operations']['timestamp'] = array(
    'title' => t('Timestamp field'), 
    'help' => t('Time at which the status was updated.'), 
    'field' => array(
      'handler' => 'views_handler_field_date', 
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Items Table
  $data['mct_shop_order_items']['table'] = array(
    'group' => t('MCT Shop'),
    'title' => t('MCT shop items'),
    'help' => t('MCT shop items on order.'),
  );
  $data['mct_shop_order_items']['table']['join'] = array(
    'mct_shop_order' => array(
      'left_field' => 'oid',
      'field' => 'oid',
    ),
    'node' => array(
      'left_field' => 'nid',
      'field' => 'item',
    ),
  );
  $data['mct_shop_order_items']['size'] = array(
    'title' => t('Size'),
    'help' => t('Item size.'),
    'field' => array(
      'handler' => 'views_handler_field', 
    ), 
  );
  $data['mct_shop_order_items']['item'] = array(
    'title' => t('Item node'),
    'help' => t('The node ID of the item.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Item'),
    ),
  );
  $data['mct_shop_order_items']['value'] = array(
    'title' => t('Value'), 
    'help' => t('Item cost.'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['mct_shop_order_items']['quantity'] = array(
    'title' => t('Quantity'), 
    'help' => t('Number of items on order.'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
