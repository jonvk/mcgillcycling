<?php

/**
 * Helper to get list of buyable items.
 * Returns an array of item nodes.
 */
function _mct_shop_get_items() {
  $sql = "SELECT n.nid FROM {node} n 
          JOIN {content_type_mct_shop_items} ct ON ct.vid = n.vid and ct.field_mct_shop_available_value = 1
          WHERE TYPE = 'mct_shop_items'";
  $result = db_query($sql);
  $items = array();
  while ($row = db_fetch_array($result)) {
    $items[] = node_load($row['nid']);
  }
  return $items;
}

/**
 * Take a filter parameter eg (oid) and return a where clause for the sql query.
 * Assumes mct_shop_order aliased as o and users table joined and aliased as u.
 */
function _mct_shop_form_filter_to_sql($filter, $value) {
  switch($filter) {
    case 'order_id':
      return array('sql' => 'o.oid = %d', 'arg' => $value);
    case 'fullname':
      return array('sql' => 'o.name like "%%%s%%"', 'arg' => $value);
    case 'username':
      return array('sql' => 'u.name = "%s"', 'arg' => $value);
    case 'order_status':
      return array('sql' => 'o.status in (%s)', 'arg' => implode(',', $value));
    case 'po':
      return array('sql' => 'po.po = %d', 'arg' => $value);
    case 'order_date_start':
      return array('sql' => 'o.timestamp >= %d', 'arg' => strtotime($value));
    case 'order_date_end':
      return array('sql' => 'o.timestamp < %d', 'arg' => strtotime($value));
  }
}

/**
 * Takes filters from $form_state['values'] and builds sql to retrieve info.
 */
function _mct_shop_form_load_selected_orders($filters) {
  $args = array();
  $sql = 'SELECT o.oid, o.uid, o.name, o.status, o.timestamp, SUM(i.quantity) pieces, SUM(i.quantity * i.value) total FROM mct_shop_order o LEFT JOIN mct_shop_order_items i on i.oid = o.oid LEFT JOIN users u ON u.uid = o.uid LEFT JOIN mct_shop_po_oid po on po.oid = o.oid';
  $groupby = ' GROUP BY o.oid, po.po';
  $where_a = _mct_shop_filters_to_sql($filters);
  $where = $where_a['sql'];
  $args = $where_a['args'];
  $sql .= $where . $groupby;
  return array('sql' => $sql, 'args' => $args);
}

function _mct_shop_filters_to_sql($filters) {
  $where = '';
  $where_a = array();
  // Filters can be NULL
  if ($filters) {
    foreach(array('fullname', 'username', 'order_id', 'order_status', 'order_date_start', 'order_date_end', 'po' => 'po') as $filter) {
      if (isset($filters[$filter]) && !is_null($filters[$filter]) && $filters[$filter] !== '' && $filters[$filter] !== array()) {
        $res =  _mct_shop_form_filter_to_sql($filter, $filters[$filter]);
        $where_a[] = $res['sql'];
        $args[] = $res['arg'];
      }
    }
    if ($where_a) {
      $where = ' WHERE ' . implode(' AND ', $where_a);
    }
  }
  return array('sql' => $where, 'args' => $args);
}

function _mct_shop_cache_filters($value=NULL) {
  static $cache = NULL;
  if (isset($value)) {
    $cache = $value;
  }
  return $cache;
}

function _mct_shop_po_op($op=NULL) {
  static $cache = 'list';
  if (isset($op)) {
    $cache = $op;
  }
  return $cache;
}
