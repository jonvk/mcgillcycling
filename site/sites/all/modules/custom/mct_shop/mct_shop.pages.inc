<?php

function mct_shop_shop($order=NULL) {
  drupal_add_css(drupal_get_path('module', 'mct_shop') . '/mct_shop.css');
  module_load_include('inc', 'mct_shop', 'mct_shop.form');
  $preamble = variable_get('mct_shop_preamble', '');
  $preamble = '<div class="mct-shop-preamble">' . check_markup($preamble, 1, FALSE) . '</div>';
  return $preamble . drupal_get_form('mct_shop_order_form', $order);
}

function mct_shop_admin() {
  $form = array(
    'mct_shop_price_multiplier' => array(
      '#type' => 'textfield',
      '#title' => t('Non-member price adjustment'),
      '#maxlenght' => 5,
      '#size' => 4,
      '#required' => TRUE,
      '#description' => t('The amount by which non-member\'s prices are multiplied. EG if non-members pay 50% extra, this should be "1.5". If the price should be the same, leave as 1.'),
      '#default_value' => variable_get('mct_shop_price_multiplier', 1.5),
    ),
    'mct_shop_preamble' => array(
      '#type' => 'textarea',
      '#title' => t('Shop preamble'),
      '#description' => t('The preamble which is displayed at the top of the shopping form.'),
      '#default_value' => variable_get('mct_shop_preamble', ''),
    ),
    'mct_shop_confirmation' => array(
      '#type' => 'textarea',
      '#title' => t('Confirmation message'),
      '#description' => t('Confirmation message when a user saves an order'),
      '#default_value' => variable_get('mct_shop_confirmation', ''),
    ),
    'mct_shop_pay_now' => array(
      '#type' => 'textarea',
      '#title' => t('"Pay Now" instructions'),
      '#description' => t('Message displayed with the Paypal Pay Now button on the order viewing page.'),
      '#default_value' => variable_get('mct_shop_pay_now', ''),
    ),

  );
  return system_settings_form($form);
}

/**
 * Non-members should pay at most 3*more and at least as much.
 */
function mct_shop_admin_validate($form, &$form_state) {
  $value = $form_state['values']['mct_shop_price_multiplier'];
  if ($value && (!is_numeric($value) || $value < 1 || $value > 3)) {
    form_set_error('mct_shop_price_multiplier', t('Please specify a valid number (between 1.00 and 3.00)'));
  }
}  

/**
 * Page callback to list ordered items with quantities.
 **/
function mct_shop_admin_orders() {
  global $user;
  module_load_include('inc', 'mct_shop', 'mct_shop.form');
  $filter_form = drupal_get_form('mct_shop_po_form');
  $op = _mct_shop_po_op();
  $filters = _mct_shop_cache_filters();
  $where = _mct_shop_filters_to_sql($filters);
  $items = mct_shop_item_orders($where);
  $header = array(t('Item'), t('Size'), t('Quantity'));
  $nid = -1;
  $odd = FALSE;
  $rows = array();
  foreach ($items as $item) {
    if ($item['nid'] != $nid) {
      $nid = $item['nid'];
      $odd = !$odd;
    }
    array_shift($item);
    if ($op != 'csv') {
      $item['title'] = l($item['title'], 'node/' . $nid);
    }
    switch($op) {
      case 'csv':
        $row = array_values($item);
        $row[] = url('node/' . $nid, array('absolute'=>TRUE));
        break;
      default:
        $row = array(
          'data' => array_values($item),
          'class' => 'item-',
        );
        $row['class'] .= $odd ? 'odd' : 'even';
    }
    $rows[] = $row;
  }
  switch ($op) {
    case 'csv':
      $poid = mct_shop_save_po($filters);
      // Print a csv
      $filename = 'orders-' . date('Y-m-d', $_SERVER['REQUEST_TIME']) . '-po-' . $poid;
      header("Content-type: text/csv");
      header("Cache-Control: no-store, no-cache");
      header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
      $outstream = fopen("php://output",'w');
      fwrite($outstream, "PO#: " . $poid . "\n");
      $header[] = 'url';
      array_unshift($rows, $header);
      foreach ($rows as $row) {
        fputcsv($outstream, $row, ',', '"');
      }
      fclose($outstream);
      exit();
    default:
      $caption = t('Orders');
      return $filter_form . theme_table($header, $rows, array(), $caption);
  }
}

function mct_shop_order_bulk_update() {
  module_load_include('inc', 'mct_shop', 'mct_shop.form');
  return drupal_get_form('mct_shop_order_bulk_update_form');
}

function mct_shop_view_order($order) {
  drupal_add_css(drupal_get_path('module', 'mct_shop') . '/mct-shop-order.css');
  $items = array();
  $total = 0;
  foreach ($order['items'] as $item) {
    $total += $item['value']*$item['quantity'];
    $items[] = theme('mct_shop_order_item', $item);
  }
  $total = round($total, 2);
  $amount = sprintf('%.2f', round(ceil(1.0325*$total)/100, 2));
  $total = theme('mct_shop_value', $total);
  $message = check_markup(variable_get('mct_shop_pay_now', ''), 1);
  return theme('mct_shop_order', $order['order'], $order['operations'], $items, $total, $amount, $message);
}
