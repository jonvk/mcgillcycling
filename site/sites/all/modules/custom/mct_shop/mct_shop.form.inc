<?php

/**
 * The summary / print to csv form.
 */
function mct_shop_po_form(&$form_state) {
  $form = array();
  $filters = $form_state['storage']['filters'];
  _mct_shop_filters_form($form, $filters);
  $form['filters']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save to CSV'),
    '#prefix' => '<span> </span><div class="mct-shop-csv-warning">' .  t('This will save a PO and save the current query to a csv. All orders which are part of this PO will be frozen.'). '<br />',
    '#suffix' => '</div>',
  );
  _mct_shop_cache_filters($filters);
  if ($form_state['storage']['filters']['op'] == t('Save to CSV')) {
    _mct_shop_po_op('csv');
  }
  $form['#submit'] = array('_mct_shop_form_po_filters_submit');
  return $form;
}

/**
 * The main shopping form. Buy items etc.
 */
function mct_shop_order_form(&$form_state, $order=NULL) {
  global $user;
  drupal_add_js(drupal_get_path('module', 'mct_shop') . '/mct-shop.js');
  $form = array();
  $items = _mct_shop_get_items();
  if ($order) {
    $form['#oid'] = $order['order']['oid'];
  }
  $values = isset($form_state['values']) ? $form_state['values'] : NULL;
  if (!$values && $order) {
    foreach($order['items'] as $item) {
      $label = 'item-' . $item['item'] . ($item['size'] ? '-sz-' . $item['size'] : '-os');
      $values[$label] = $item['quantity'];
    }
    foreach (array('name' => 'name', 'phone' => 'phone', 'shipping'=>'address', 'email' => 'email') as $key => $label) {
      $values[$label] = $order['order'][$key];
    }
  }
  if (!$values && !$order) {
    profile_load_profile($user);
    $default_email = $user->mail;
    $default_name = $user->profile_name;
  }
  else {
    $default_email = isset($values['email']) ? $values['email'] : '';
    $default_name = isset($values['name']) ? $values['name'] : '';
  }
  $form['items'] = array(
    '#type' => 'fieldset',
  );
  $form['general'] = array(
    '#type' => 'fieldset',
    'personal' => array(
      '#type' => 'fieldset',
      'name' => array(
        '#type' => 'textfield',
        '#title' => t('Full name'),
        '#size' => 24,
        '#maxlength' => 63,
        '#required' => TRUE,
        '#default_value' => $default_name,
      ),
      'email' => array(
        '#type' => 'textfield',
        '#title' => t('Email address'),
        '#description' => t('The email address we will use for order related communications.'),
        '#size' => 24,
        '#maxlength' => 63,
        '#required' => TRUE,
        '#default_value' => $default_email,
        '#element_validate' => '_mct_shop_email_validate',
      ),
      'phone' => array(
        '#type' => 'textfield',
        '#title' => t('Phone number'),
        '#description' => '123-456-7890',
        '#size' => 12,
        '#maxlength' => 14,
        '#element_validate' => '_mct_shop_phone_validate',
        '#default_value' => isset($values['phone']) ? $values['phone'] : '',
      ),
      'comment' => array(
        '#type' => 'textarea',
        '#title' => t('Notes'),
        '#cols' => 24,
        '#rows' => 3,
        '#description' => t('Special instructions, questions, etc'),
        '#default_value' => isset($values['comment']) ? $values['comment'] : '',
      ),
    ),
    'shipping' => array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Shipping'),
      'address' => array(
        '#type' => 'textarea',
        '#title' => t('Shipping address'),
        '#cols' => 24,
        '#rows' => 4,
        '#description' => t('Only necessary if you require shipping.'),
        '#default_value' => isset($values['address']) ? $values['address'] : '',
      ),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Send order"),
  );
  foreach( $items as $item ) {
    $form['items']['item-' . $item->nid] = _mct_shop_form_item($item, $values);
  }
  return $form;
}

/**
 * Takes an item node and returns pre and post wrappers plus the form elements
 * for the order form.
 */
function _mct_shop_form_item($node, $values=NULL) {
  global $user;
  // Non-team member prices get mulitplied by price_multiplier
  $team = in_array('team', $user->roles);
  $price_multiplier = $team ? 1 : variable_get('mct_shop_price_multiplier', 1);

  $form = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Quantity'),
  );

  // Wrapper
  $form['#prefix'] = '<div id="mct-shop-item-' . $node->nid . '" class="mct-shop-item">';
  $form['#suffix'] = '</div>';
  // Image
  $form['#prefix'] .= '<div class="mct-shop-list-image">';
  if (isset($node->field_mct_shop_image[0])) {
    $img = $node->field_mct_shop_image[0];
    $form['#prefix'] .= theme('imagecache', 'mct_shop_item', $img['filepath'], $img['data']['description'],  $node->title);
  }
  $form['#prefix'] .= '</div>'; // End image
  $form['#prefix'] .= '<div class="mct-shop-list-text">';
  $form['#prefix'] .= '<div class="mct-shop-list-title">';
  $form['#prefix'] .= $node->title;
  $form['#prefix'] .= '</div>'; // End title
  $form['#prefix'] .= '<div class="mct-shop-list-body">';
  $form['#prefix'] .= $node->body;
  $form['#prefix'] .= '</div>'; // End body
  $form['#prefix'] .= '<div class="mct-shop-list-price">';
  $form['#prefix'] .= sprintf("%.2f$", $price_multiplier * $node->field_mct_shop_price[0]['value']);
  $form['#prefix'] .= '</div>'; // End price
  $form['#prefix'] .= '</div>'; // End text

  // Quantity / Sizes
  $form['#prefix'] .= '<div class="mct-shop-list-quantity">';
  $form['#suffix'] = '</div>' . $form['#suffix'];
  if (isset($node->field_mct_shop_sizes[0]['value'])) {
    foreach($node->field_mct_shop_sizes as $size) {
      $label = 'item-' . $node->nid . '-sz-' . $size['value'];
      $quantity = 0;
      if (isset($values) && isset($values[$label])) {
        $quantity = $values[$label];
      }
      $form[$label] = array(
        '#type' => 'textfield',
        '#title' => $size['value'],
        '#default_value' => $values[$label],
        '#size' => 2,
        '#maxlength' => 3,
        '#element_validate' => array('_mct_shop_quantity_validate'),
      );
    }
  }
  else {
    $label = 'item-' . $node->nid . '-os';
    if (isset($values) && isset($values[$label])) {
      $quantity = $values[$label];
    }
    $form[$label] = array(
      '#type' => 'textfield',
      '#title' => $size,
      '#default_value' => $quantity,
      '#size' => 3,
      '#maxlength' => 3,
      '#element_validate' => '_mct_shop_quantity_validate',
    );
  }
  return $form;
}

/**
 * Print form for bulk updates (confirm, cancel, etc) of orders.
 */
function mct_shop_order_bulk_update_form(&$form_state) {
  $items = mct_shop_item_orders();
  $filters = $form_state['storage']['filters'];
  $form = array();

  // Add filters to form
  _mct_shop_filters_form($form, $filters);

  $form['results'] = array(
    '#type' => 'fieldset',
    '#title' => 'Orders',
  );

  // Add update options
  $form['results']['update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update selected orders'),
    '#description' => t('Select the orders you want to update and set the status and an optional status.'),
  );
  $form['results']['update']['new_status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t("Update selected orders' status."),
    '#options' => _mct_shop_status_list(),
    '#default_value' => 0,
  );
  $form['results']['update']['comment'] = array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#title' => t('Note'),
    '#description' => t("Optional note with order update."),
  );
  $form['results']['update']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update status'),
  );

  // Get and display orders based on filters.
  $sql = _mct_shop_form_load_selected_orders($filters);
  $results = db_query($sql['sql'], $sql['args']);
  $headers = array('Order ID', 'Status', 'Username', 'Name', 'Quantity', 'Total', 'Date', 'Update');
  $header = implode('</th><th>', $headers);
  $form['results']['orders'] = array(
    '#prefix' => '<table class="mct-order-list"><tbody><tr><th>' . $header . '</th></tr>' . "\n",
    '#suffix' => '</tbody></table>',
  );
  while ($row = db_fetch_array($results)) {
    $form['results']['orders']['order-' . $row['oid']] = array(
      '#prefix' => '<tr>' . theme('mct_shop_order_row', $row) . '<td>',
      '#suffix' => '</td></tr>',
      '#type' => 'checkbox',
    );
  }
  $form['#submit'] = array('_mct_shop_bulk_update_submit');
  return $form;
}

function _mct_shop_filters_form(&$form, $filters) {
  drupal_add_css(drupal_get_path('module', 'mct_shop') . '/mct-shop.css');
  // Build filters
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Refine selection'),
    '#attributes' => array('class' => 'mct-shop-filters'),
  );
  $form['filters']['order_id'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 8,
    '#title' => t('Order ID'),
    '#set_default' => TRUE,
  );
  $form['filters']['order_status'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#size' => 5,
    '#title' => t('Order Status'),
    '#options' => _mct_shop_status_list(),
    '#set_default' => TRUE,
  );
  $form['filters']['po'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 8,
    '#title' => t('PO'),
    '#set_default' => TRUE,
  );
  $form['filters']['username'] = array(
    '#type' => 'textfield',
    '#maxlength' => 60,
    '#size' => 16,
    '#autocomplete_path' => 'user/autocomplete',
    '#title' => t('Username'),
    '#set_default' => TRUE,
  );
  $form['filters']['fullname'] = array(
    '#type' => 'textfield',
    '#maxlength' => 60,
    '#size' => 16,
    '#title' => t('Name'),
    '#set_default' => TRUE,
  );
  $form['filters']['order_date_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('Ordered after'),
    '#description' => t('Date after which the order was submited (inclusive).'),
    '#set_default' => TRUE,
  );
  $form['filters']['order_date_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('Ordered by'),
    '#description' => t('Date by which the order was submited (exclusive).'),
    '#set_default' => TRUE,
  );
  foreach ($form['filters'] as $key => &$element) {
    if (isset($element['#set_default']) && $element['#set_default'] === TRUE) {
      $element['#default_value'] = _mct_shop_form_get_value($filters, $key);
      unset($element['#set_default']);
    }
  }
  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply filters'),
    '#prefix' => '<span> </span>',
  );
 }

/**
 ** Submit and validate handlers
 **/

/**
 * Make sure the quantity is a positive integer.
 */
function _mct_shop_quantity_validate($element, &$form_state) {
  if ($element['#value'] && !preg_match('/^\d{1,3}$/', $element['#value'])) {
    form_error($element, t('Please specify a valid quantity'));
  }
}

/**
 * Validate phone numbers. (American - we ain't about to call Europe).
 */
function _mct_shop_phone_validate($element, &$form_state) {
  if ($element['#value'] && !preg_match('/^(\d-)?\d{3}-\d{3}-\d{4}$/', $element['#value'])) {
    form_error($element, t('Please leave a valid (American) phone number.'));
  }
}

/**
 * Validate emails.
 */
function _mct_shop_email_validate($element, &$form_state) {
  if (!valid_email_address($element['#value'])) {
    form_error($element, t('Please submit a valid email address.'));
  }
}


/**
 * Process and stash filter results.
 */
function _mct_shop_form_po_filters_submit($form, &$form_state) {
  _mct_shop_form_save_filters($form_state);
}

/**
 * Submit handler for shopping form.
 */
function mct_shop_order_form_submit($form, &$form_state) {
  global $user;
  $save = FALSE;
  $order = array();

  // Non-team member prices get mulitplied by price_multiplier
  $team = in_array('team', $user->roles);
  $price_multiplier = $team ? 1 : variable_get('mct_shop_price_multiplier', 1);

  $comment = isset($form_state['values']['comment']) ? $form_state['values']['comment'] : '';
  $order['operations'] = array(_mct_shop_operation(MCT_SHOP_STATUS_NEW, $comment));
  // Process items
  $items = array();
  foreach ($form_state['values'] as $key => $value) {
    if (substr($key, 0, 5) == 'item-') {
      foreach ($value as $key => $quantity) {
        if ($quantity) {
          $save = TRUE;
          $keys = explode('-', $key, 4);
          array_shift($keys);
          $nid = array_shift($keys);
          $node = node_load($nid);
          $item = array(
            'item' => $nid,
            'quantity' => $quantity,
            'value' => (int) (100 * $price_multiplier * $node->field_mct_shop_price[0]['value']),
          );
          $value_delta = array_shift($keys);
          if ($value_delta != 'os') {
            $item['size'] = array_shift($keys);
          }
          $items[] = $item;
        }
      }
    }
  }
  $order['items'] = $items;
  $order['order'] = array(
    'uid' => $user->uid,
    'name' => $form_state['values']['name'],
    'email' => $form_state['values']['email'],
    'phone' => $form_state['values']['phone'],
    'shipping' => $form_state['values']['address'],
  );
  if (isset($form['#oid'])) {
    $order['order']['oid'] = $form['#oid'];
    $order['operations'][0]['status'] = MCT_SHOP_STATUS_EDITED;

  }
  else {
    $order['order']['timestamp'] = $_SERVER['REQUEST_TIME'];
  }
  if ($save) {
    mct_shop_save_order($order);
  }
  $form_state['redirect'] = '/shop/order/view/' . $order['order']['oid'];
}

function _mct_shop_bulk_update_submit($form, &$form_state) {
    _mct_shop_form_save_filters($form_state);
  if ($form_state['values']['submit'] == t('Update status')) {
    $status = $form_state['values']['new_status'];
    $comment = $form_state['values']['comment'];
    foreach($form_state['values'] as $key => $value) {
      if (substr($key, 0, 6) == 'order-' and $value === 1) {
        $oid = (int) substr($key, 6);
        $order = mct_shop_load_order($oid);
        $operation = _mct_shop_operation($status, $comment, $oid);
        $order['operations'][] = $operation;
        mct_shop_save_order($order, TRUE);
      }
    }
  }
}

/**
 ** Helpers
 **/

/**
 * Save form values to form storage for post-redirect requests.
 */
function _mct_shop_form_save_filters(&$form_state) {
  $form_state['storage'] = array(
    'filters' => $form_state['values'],
  );
}

/**
 * Extract value from form_state array.
 */
function _mct_shop_form_get_value($values, $key) {
  if (isset($values) && isset($values[$key])) {
    return $values[$key];
  }
  return NULL;
}
